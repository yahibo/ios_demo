//
//  ViewController.m
//  dynamic
//
//  Created by 杨海波 on 2017/8/19.
//  Copyright © 2017年 杨海波. All rights reserved.
//

#import "ViewController.h"
#import "DynamicCell.h"
#import "OutputView.h"

@interface ViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *dataArr;//数据源
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = backColor;
    UIView *back = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Width, 64)];
    back.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:back];
    UILabel *label = [MyBaseView myLabelFrame:CGRectMake(0, 20, Width, 44) text:@"动态" textColor:nil backgroudColor:nil font:[UIFont systemFontOfSize:18]];
    label.textAlignment = NSTextAlignmentCenter;
    [back addSubview:label];
    //创建tableview
    self.tableview = [MyBaseView myTableViewFrame:CGRectMake(0, 64, Width, Height-64) style:UITableViewStyleGrouped delegate:self];
    self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableview registerClass:[DynamicHeaderView class] forHeaderFooterViewReuseIdentifier:@"DynamicHeaderView"];
    [self.tableview registerClass:[DynamicFooterView class] forHeaderFooterViewReuseIdentifier:@"DynamicFooterView"];
    [self.tableview registerClass:[DynamicPictureCell class] forCellReuseIdentifier:@"DynamicPictureCell"];
    [self.tableview registerClass:[DynamicButtonCell class] forCellReuseIdentifier:@"DynamicButtonCell"];
    [self.tableview registerClass:[DynamicCommentCell class] forCellReuseIdentifier:@"DynamicCommentCell"];
    [self.view addSubview:self.tableview];
    
    dataArr = [NSMutableArray array];
    NSDictionary *dic1 = @{@"user_info":@{@"name":@"杨洋",@"time":@"1503124127",@"company":@"标识设计"},
                          @"content":@"一男子练刀8年，在当地渐渐没了对手，于是决定出游以武会友。华山脚下，他看见一座建筑深藏于幽谷之间，上书“华山派”。",
                          @"picture":@[],
                          @"comment":@[@"杨洋 回复 张月：说的好",@"张月 回复 杨洋：一般般",@"杨洋 回复 张月：非常烂一般般一般般一般般一般般一般般一般般一般般一般般一般般"]};
    NSDictionary *dic2 = @{@"user_info":@{@"name":@"杨洋",@"time":@"1503124127",@"company":@"标识设计"},
                          @"content":@"一男子练刀8年，在当地渐渐没了对手，于是决定出游以武会友。华山脚下，他看见一座建筑深藏于幽谷之间，上书“华山派”。一男子练刀8年，在当地渐渐没了对手，于是决定出游以武会友。华山脚下，他看见一座建筑深藏于幽谷之间，上书“华山派”一男子练刀8年，在当地渐渐没了对手，于是决定出游以武会友。华山脚下，他看见一座建筑深藏于幽谷之间，上书“华山派”",
                          @"picture":@[@"",@"",@"",@"",@"",@""],
                          @"comment":@[@"杨洋 回复 张月：说的好说的好说的好说的好说的好",@"张月：一般般",@"杨洋 回复 张月：非常烂"]};
    NSDictionary *dic3 = @{@"user_info":@{@"name":@"杨洋",@"time":@"1503124127",@"company":@"标识设计"},
                           @"content":@"好吧",
                           @"picture":@[@"",@""],
                           @"comment":@[@"杨洋 回复 张月：说的好",@"张月：一般般一般般一般般一般般一般般一般般一般般一般般一般般",@"杨洋 回复 张月：非常烂"]};
    NSDictionary *dic4 = @{@"user_info":@{@"name":@"杨洋",@"time":@"1503124127",@"company":@"标识设计"},
                           @"content":@"好吧",
                           @"picture":@[@"",@""],
                           @"comment":@[@"杨洋 回复 张月：说的好",@"张月：一般般一般般一般般一般般一般般一般般一般般一般般一般般",@"杨洋 回复 张月：非常烂"]};
    NSDictionary *dic5 = @{@"user_info":@{@"name":@"杨洋",@"time":@"1503124127",@"company":@"标识设计"},
                           @"content":@"好吧杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好",
                           @"picture":@[@"",@""],
                           @"comment":@[@"杨洋 回复 张月：说的好杨洋 回复 张月：说的好",@"张月：一般般一般般一般般一般般一般般一般般一般般一般般一般般",@"杨洋 回复 张月：非常烂杨洋 回复 张月：说的好杨洋 回复 张月：说的好杨洋 回复 张月：说的好"]};
    for(int i = 0; i < 20; i++){
        if (i%5==0) {
            [dataArr addObject:dic1];
        }else if (i%5==1) {
            [dataArr addObject:dic2];
        }else if (i%5==2) {
            [dataArr addObject:dic3];
        }else if (i%5==3) {
            [dataArr addObject:dic4];
        }else if (i%5==4) {
            [dataArr addObject:dic5];
        }
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return dataArr.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSDictionary *info = dataArr[section];
    NSArray *commentArr = info[@"comment"];
    return commentArr.count+2;//评论cell个数+1个picture+1个分享按钮
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *info = dataArr[indexPath.section];
    if (indexPath.row==0) {//图片
        DynamicPictureCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DynamicPictureCell" forIndexPath:indexPath];
        [cell setImgArr:info[@"picture"]];
        return cell;
    }else if(indexPath.row==1){//分享按钮
        DynamicButtonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DynamicButtonCell" forIndexPath:indexPath];
        [cell.shareButton addTarget:self action:@selector(shareBtn:) forControlEvents:UIControlEventTouchUpInside];
        [cell.commentButton addTarget:self action:@selector(commentBtn:) forControlEvents:UIControlEventTouchUpInside];
        [cell.zanButton addTarget:self action:@selector(zanBtn:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }else{//评论
        DynamicCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DynamicCommentCell" forIndexPath:indexPath];
        NSArray *commentArr = info[@"comment"];
        NSString *comment = commentArr[indexPath.row-2];
        [cell setContent:comment];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *info = dataArr[indexPath.section];
    if (indexPath.row>1) {
        NSArray *commentArr = info[@"comment"];
        NSString *comment = commentArr[indexPath.row-2];
        NSLog(@"comment:%@",comment);
        
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    DynamicHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"DynamicHeaderView"];
    NSDictionary *info = dataArr[section];
    [headerView.moreButton addTarget:self action:@selector(moreBtn:) forControlEvents:UIControlEventTouchUpInside];
    headerView.section = section;
    [headerView setDataDic:info];
    return headerView;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    DynamicFooterView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"DynamicFooterView"];
    [headerView.commentButton addTarget:self action:@selector(commentBtn:) forControlEvents:UIControlEventTouchUpInside];
    return headerView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *info = dataArr[indexPath.section];
    if(indexPath.row==0){//图片高度
        return [DynamicPictureCell getPictureHeight:[info[@"picture"] count]];
    }else if(indexPath.row==1){//按钮
        return 50*rateh;
    }else{//评论
        NSArray *commentArr = info[@"comment"];
        NSString *comment = commentArr[indexPath.row-2];
        return [DynamicCommentCell getCellHeightWithContent:comment];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    NSDictionary *info = dataArr[section];
    NSString *content = info[@"content"];
    CGFloat height = [DynamicHeaderView getHeaderViewHeightWithContent:content];
    return height;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 60*rateh;
}

//更多
-(void)moreBtn:(UIButton*)button{
    UIWindow * window = [[[UIApplication sharedApplication] delegate] window];
    CGRect rect = [button convertRect:[button bounds] toView:window];
    Direction direction = OutputViewDirectionRight;
    CGFloat origin_y = rect.origin.y+rect.size.height;
    if (rect.origin.y+rect.size.height/2>(Height-64)/2) {
        direction = OutputViewDirectionDownRight;
        origin_y = rect.origin.y;
    }
    NSArray *dataArray = @[@{@"title":@"关注",@"img":@"follow_icon"},
                  @{@"title":@"收藏",@"img":@"collection_icon"},
                  @{@"title":@"隐藏此条动态",@"img":@"hide_dynamic_icon"},
                  @{@"title":@"不看他的动态",@"img":@"not_look_icon"},
                  @{@"title":@"举报",@"img":@"report_icon"}];
    OutputView *outputV = [[OutputView alloc] initWithPoint:CGPointMake(Width, origin_y) rowHeigth:50*rateh direction:direction dataArray:dataArray];
    [outputV setRowWidth:Width];//重新设置弹框宽
    [outputV setArrow_x:11*rateh];//设置箭头位置
    outputV.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundView_alpha"]];
    outputV.tableView.backgroundColor = [UIColor whiteColor];
    outputV.celltextColor = [UIColor blackColor];
    outputV.textAlignment = NSTextAlignmentLeft;
    outputV.tableView.layer.cornerRadius = 0;
    [outputV popForTwo];
    outputV.resultBlock = ^(id result){
        NSLog(@"result:%@",result);
    };
}
//点击事件
-(void)shareBtn:(UIButton*)button{
    NSLog(@"分享");
}
//评论
-(void)commentBtn:(UIButton*)button{
    NSLog(@"评论");
}
//点赞
-(void)zanBtn:(UIButton*)button{
    NSLog(@"点赞");
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end







