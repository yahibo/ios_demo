//
//  DynamicCell.m
//  dynamic
//
//  Created by 杨海波 on 2017/8/19.
//  Copyright © 2017年 杨海波. All rights reserved.
//

#import "DynamicCell.h"

@implementation DynamicCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

//传入字符串、字体大小以及尺寸限制，得到字符串对应的尺寸
+ (CGSize)sizeWithString:(NSString *)string font:(UIFont *)font size:(CGSize)size{
    if ([self isStringEmpty:string]) return CGSizeMake(0, 0);
    CGRect rect = [string boundingRectWithSize:size//限制最大的宽度和高度
                                       options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesFontLeading  |NSStringDrawingUsesLineFragmentOrigin//采用换行模式
                                    attributes:@{NSFontAttributeName: font}//传人的字体字典
                                       context:nil];
    
    return rect.size;
}
+ (BOOL)isStringEmpty:(NSString *) string {
    if ([string isKindOfClass:[NSNull class]]) {
        return YES ;
    }
    if (string == nil) {
        return YES ;
    }
    if (string .length == 0 ||
        [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        return YES ;
    }
    return NO ;
}
//设置行间距
/*
 return  @"attributeStr":attributeStr,
 @"width":width,
 @"height":height
 */
+(NSDictionary*)attributedString:(NSString*)string Font:(UIFont*)font withSize:(CGSize)size withSpace:(NSArray*)spaceArr{
    if (![string isKindOfClass:[NSString class]]) {
        string = @"";
    }
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = [spaceArr[0] floatValue]; //设置行间距
    paraStyle.hyphenationFactor = 0.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    //设置字间距 NSKernAttributeName:@1.5f
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:spaceArr[1]};
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:string attributes:dic];
    CGSize getSize = [string boundingRectWithSize:CGSizeMake(size.width, size.height) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    
    NSString *width = [NSString stringWithFormat:@"%f",getSize.width];
    NSString *height = [NSString stringWithFormat:@"%f",getSize.height];
    NSDictionary *returnDic = @{@"attributeStr":attributeStr,
                                @"width":width,
                                @"height":height
                                };
    return returnDic;
}

@end

//图片加载cell
@implementation DynamicPictureCell
{
    NSMutableArray *imgObjArr;
}
#define picNumForRow 3  //图片每行个数
#define picSpace 5//图片间距
#define picWidth (Width-20-(picNumForRow-1)*picSpace)/picNumForRow  //图片的宽高

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIImageView *curImgv;
        imgObjArr = [NSMutableArray array];
        for (int i = 0; i < 9; i++) {
            UIImageView *imgv = [[UIImageView alloc] init];
            imgv.tag = i;
            imgv.clipsToBounds = YES;
            imgv.userInteractionEnabled = YES;
            imgv.contentMode = UIViewContentModeScaleAspectFill;
            [self addSubview:imgv];
            [imgv mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(picWidth, picWidth));
                if (i==0) {
                    make.left.mas_equalTo(self.mas_left).offset(10);
                    make.top.mas_equalTo(self.mas_top);
                }else{
                    if (i%3==0) {
                        make.left.mas_equalTo(self.mas_left).offset(10);
                        make.top.mas_equalTo(curImgv.mas_bottom).offset(picSpace);
                    }else{
                        make.left.mas_equalTo(curImgv.mas_right).offset(picSpace);
                        make.top.mas_equalTo(curImgv.mas_top);
                    }
                }
                
            }];[curImgv.superview layoutIfNeeded];
            curImgv = imgv;
            [imgObjArr addObject:imgv];
            UITapGestureRecognizer *tapPicture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapEvent:)];
            [imgv addGestureRecognizer:tapPicture];
        }
    }
    return self;
}
//获取数据
-(void)setImgArr:(NSArray *)imgArr{
    for (int i = 0; i < 9; i++) {
        UIImageView *img = imgObjArr[i];
        if (i < imgArr.count) {
            img.image=[UIImage imageNamed:@"head_2"];
        }else{
            img.image=[UIImage imageNamed:@""];
        }
    }
}
#pragma mark- 查看图片
-(void)tapEvent:(UITapGestureRecognizer*)tap{

}
//获取图片的高度
+(CGFloat)getPictureHeight:(NSInteger)picNum{
    CGFloat height = (picNum/picNumForRow+(picNum%picNumForRow!=0?1:0))*(picWidth+picSpace);
    return height;
}
@end

//按钮处理cell
@implementation DynamicButtonCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //点赞数量
        NSArray *array = @[@"分享",@"0",@"0"];
        NSArray *imgArr = @[@"share_gray",@"find_comment",@"find_like"];
        for (int i=0; i<array.count; i++) {
            CustomButton *button = [MyBaseView myButtonFrame:CGRectNull text:array[i] textColor:[UIColor grayColor] backgroudColor:nil font:[UIFont systemFontOfSize:16*rateh]];
            button.titleEdgeInsets = UIEdgeInsetsMake(0, 20*rateh+5*ratew, 0, 0);
            [self addSubview:button];
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(Width/3, 40*rateh));
                make.left.mas_equalTo(self.mas_left).offset(i*Width/3);
                make.top.mas_equalTo(self.mas_top);
            }];
            UIImageView *leftImagev = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgArr[i]]];
            [button addSubview:leftImagev];
            [leftImagev mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(20*rateh, 20*rateh));
                make.right.mas_equalTo(button.titleLabel.mas_left).offset(-5*ratew);
                make.centerY.mas_equalTo(button);
            }];
            if (i==0) {
                _shareButton = button;
            }else if (i==1){
                _commentButton = button;
            }else{
                _zanButton = button;
                _likeView = leftImagev;
            }
        }
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = layerColor;
        [self addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Width-20, 0.5));
            make.centerX.mas_equalTo(self);
            make.top.mas_equalTo(_shareButton.mas_bottom);
        }];
    }
    return self;
}
@end

//评论cell
@implementation DynamicCommentCell
{
    UILabel *contentLabel;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //详情
        contentLabel = [MyBaseView myLabelFrame:CGRectNull text:@"content" textColor:[UIColor blackColor] backgroudColor:nil font:[UIFont systemFontOfSize:font14]];
        contentLabel.numberOfLines=0;
        [self addSubview:contentLabel];
        [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(Width-20);
            make.left.mas_equalTo(self.mas_left).offset(10);
            make.centerY.mas_equalTo(self);
        }];
    }
    return self;
}
-(void)setContent:(NSString *)content{
    contentLabel.text = content;
    CGFloat height = [DynamicCommentCell getCellHeightWithContent:content];
    [contentLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(height);
    }];
    
    NSRange range1 = [content rangeOfString:@"杨洋"];
    NSRange range2 = [content rangeOfString:@"张月"];
    contentLabel.attributedText = [self attributedString:content lastRange:range1 lastRange:range2 color:RGB(40, 90, 160)];
}

//改变字体大小
-(NSMutableAttributedString *)attributedString:(NSString*)string lastRange:(NSRange)range1 lastRange:(NSRange)range2 color:(UIColor*)color {
    NSMutableAttributedString *titStr = [[NSMutableAttributedString alloc] initWithString:string];
    [titStr addAttribute:NSForegroundColorAttributeName value:color range:range1];
    [titStr addAttribute:NSForegroundColorAttributeName value:color range:range2];
    return titStr;
}
//获取评论cell的高度
+(CGFloat)getCellHeightWithContent:(NSString*)content{
    CGSize size  = [self sizeWithString:content font:[UIFont systemFontOfSize:font14] size:CGSizeMake(Width-20, Height)];
    return size.height+10*rateh;
}

@end



//头视图
@implementation DynamicHeaderView

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = [[UIView alloc] init];
        self.backgroundView.backgroundColor = [UIColor whiteColor];
        //头像
        _headImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"head_1"]];
        [self addSubview:_headImageView];
        [_headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(40*rateh, 40*rateh));
            make.left.mas_equalTo(self.mas_left).offset(10);
            make.top.mas_equalTo(self.mas_top).offset(10*rateh);
        }];
        //标题
        _nameLabel = [MyBaseView myLabelFrame:CGRectNull text:@"title" textColor:[UIColor blackColor] backgroudColor:nil font:[UIFont systemFontOfSize:16*rateh]];
        [self addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Width/2, 20*rateh));
            make.left.mas_equalTo(_headImageView.mas_right).offset(5);
            make.top.mas_equalTo(_headImageView.mas_top);
        }];
        //图标
        _authImgv = [[UIImageView alloc] init];
        [self addSubview:_authImgv];
        [_authImgv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(12*rateh, 12*rateh));
            make.left.mas_equalTo(_nameLabel.mas_right).offset(5);
            make.centerY.mas_equalTo(_nameLabel);
        }];
        //时间
        _timeLabel = [MyBaseView myLabelFrame:CGRectNull text:@"time" textColor:[UIColor grayColor] backgroudColor:nil font:[UIFont systemFontOfSize:14*rateh]];
        _timeLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:_timeLabel];
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Width/2, 20*rateh));
            make.right.mas_equalTo(self.mas_right).offset(-10-40*rateh);
            make.centerY.mas_equalTo(_nameLabel);
        }];
        //公司名称
        _companyLabel = [MyBaseView myLabelFrame:CGRectNull text:@"company" textColor:[UIColor grayColor] backgroudColor:nil font:[UIFont systemFontOfSize:14*rateh]];
        [self addSubview:_companyLabel];
        [_companyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Width*2/3, 20*rateh));
            make.left.mas_equalTo(_headImageView.mas_right).offset(5);
            make.bottom.mas_equalTo(_headImageView.mas_bottom);
        }];
        //更多
        _moreButton = [MyBaseView myButtonFrame:CGRectNull text:@"" textColor:[UIColor grayColor] backgroudColor:nil font:[UIFont systemFontOfSize:font16]];
        [_moreButton setImage:[UIImage imageNamed:@"find_more_btn"] forState:UIControlStateNormal];
        [_moreButton setImageEdgeInsets:UIEdgeInsetsMake(11*rateh, 18*rateh, 11*rateh, 19*rateh)];
        [self addSubview:_moreButton];
        [_moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(40*rateh, 40*rateh));
            make.right.mas_equalTo(self.mas_right).offset(-5);
            make.centerY.mas_equalTo(_headImageView);
        }];
        
        //详情
        _contentLabel = [MyBaseView myLabelFrame:CGRectNull text:@"content" textColor:[UIColor blackColor] backgroudColor:nil font:[UIFont systemFontOfSize:16*rateh]];
        _contentLabel.numberOfLines=0;
        [self addSubview:_contentLabel];
        [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(Width-20);
            make.left.mas_equalTo(self.mas_left).offset(10);
            make.top.mas_equalTo(_headImageView.mas_bottom).offset(10*rateh);
        }];
    }
    return self;
}
-(void)setDataDic:(NSDictionary *)dataDic{
    self.nameLabel.text = dataDic[@"user_info"][@"name"];
    self.timeLabel.text = [NSDate timeTurnString:[dataDic[@"user_info"][@"time"] intValue]];
    NSString *content = dataDic[@"content"];
    NSDictionary *attribute = [DynamicCell attributedString:content Font:[UIFont systemFontOfSize:font16] withSize:CGSizeMake(Width-20, MAXFLOAT) withSpace:@[@5,@1.2]];
    self.contentLabel.attributedText = attribute[@"attributeStr"];
    [self.contentLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo([attribute[@"height"] floatValue]+2);
    }];
    NSArray *anonymousArr = @[@"head_1",@"head_11",@"head_21",@"head_31",@"head_41",
                     @"head_2",@"head_12",@"head_22",@"head_32",@"head_42",
                     @"head_3",@"head_13",@"head_23",@"head_33",@"head_43",
                     @"head_4",@"head_14",@"head_24",@"head_34",@"head_44",
                     ];
    int num = (int)_section%(anonymousArr.count);
    _headImageView.image = [UIImage imageNamed:anonymousArr[num]];
}
//获取头视图的高度
+(CGFloat)getHeaderViewHeightWithContent:(NSString*)content{
    NSDictionary *attribute = [DynamicCell attributedString:content Font:[UIFont systemFontOfSize:font16] withSize:CGSizeMake(Width-20, MAXFLOAT) withSpace:@[@5,@1.2]];
    return [attribute[@"height"] floatValue] + 70*rateh;
}

@end


//尾部视图
@implementation DynamicFooterView

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        UIView *bakcView = [[UIView alloc] init];
        bakcView.backgroundColor = [UIColor whiteColor];
        [self addSubview:bakcView];
        [bakcView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Width, 50*rateh));
            make.centerX.mas_equalTo(self);
            make.top.mas_equalTo(self.mas_top);
        }];
        //评论按钮
        _commentButton = [MyBaseView myButtonFrame:CGRectNull text:@"评论" textColor:[UIColor grayColor] backgroudColor:nil font:[UIFont systemFontOfSize:14*rateh]];
        _commentButton.layer.cornerRadius = 2;
        _commentButton.layer.borderWidth = 0.5;
        _commentButton.layer.borderColor = layerColor.CGColor;
        _commentButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_commentButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
        [self addSubview:_commentButton];
        [_commentButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Width-20, 30*rateh));
            make.centerX.mas_equalTo(bakcView);
            make.bottom.mas_equalTo(bakcView.mas_bottom).offset(-10*rateh);
        }];
    }
    return self;
}

@end






