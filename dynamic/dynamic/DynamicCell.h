//
//  DynamicCell.h
//  dynamic
//
//  Created by 杨海波 on 2017/8/19.
//  Copyright © 2017年 杨海波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DynamicCell : UITableViewCell

@property (nonatomic,strong) NSDictionary *dataDic;//数据

@end


//图片cell
@interface DynamicPictureCell : DynamicCell

@property (nonatomic,strong) NSArray *imgArr;
//获取图片的高度
+(CGFloat)getPictureHeight:(NSInteger)picNum;

@end


//按钮cell
@interface DynamicButtonCell : DynamicCell

@property (nonatomic,strong) CustomButton *shareButton;
@property (nonatomic,strong) CustomButton *zanButton;
@property (nonatomic,strong) CustomButton *commentButton;
@property (nonatomic,strong) UIImageView *likeView;

@end


//评论cell
@interface DynamicCommentCell : DynamicCell
@property (nonatomic,strong) NSString *content;
//获取评论cell的高度
+(CGFloat)getCellHeightWithContent:(NSString*)content;
@end


//头视图
@interface DynamicHeaderView : UITableViewHeaderFooterView

@property (nonatomic,strong) NSDictionary *dataDic;//数据
@property (nonatomic,strong) UIImageView *headImageView;//头像
@property (nonatomic,strong) UILabel *nameLabel;
@property (nonatomic,strong) UIImageView *authImgv;//认证图标
@property (nonatomic,strong) UILabel *companyLabel;
@property (nonatomic,strong) UILabel *timeLabel;
@property (nonatomic,strong) UIButton *moreButton;
@property (nonatomic,strong) UILabel *contentLabel;
@property (nonatomic,assign) NSInteger section;
//获取头视图的高度
+(CGFloat)getHeaderViewHeightWithContent:(NSString*)content;

@end

//尾视图
@interface DynamicFooterView : UITableViewHeaderFooterView

@property (nonatomic,strong) UIButton *commentButton;//评论按钮

@end






