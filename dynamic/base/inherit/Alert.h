//
//  Alert.h
//  happiness
//
//  Created by 杨海波 on 2016/12/17.
//  Copyright © 2016年 杨海波. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^resultBlock)(id result);
@interface Alert : UIAlertController
/*提示框：没有选择按钮*/
+(void)alertTitle:(NSString*)title message:(NSString*)message timeInterval:(NSTimeInterval)time;
/*提示框：没有选择按钮 返回信息*/
+(void)alertTitle:(NSString *)title message:(NSString *)message withBlock:(resultBlock)block;
/*提示框：多选择框*/
+(void)alertTitle:(NSString *)title message:(NSString *)message titleArr:(NSArray*)titleArr withBlock:(resultBlock)block;
/*提示框：多选择框*/
+(void)actionSheet:(NSString *)title message:(NSString *)message titleArr:(NSArray *)titleArr withBlock:(resultBlock)block;

/*
 * 小弹框 1s后消失
 */
+(void)alertWithButtonTitle:(NSString*)title;





/*************非导航条下的视图弹框****************/
/*
 提示框
 title  提示标题
 message    提示信息
 controller 控制器
 time   一定时间后返回数据
 */
+(void)alertTitle:(NSString *)title message:(NSString *)message controller:(id)controller withBlock:(resultBlock)block;
@end
