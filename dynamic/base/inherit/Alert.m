//
//  Alert.m
//  happiness
//
//  Created by 杨海波 on 2016/12/17.
//  Copyright © 2016年 杨海波. All rights reserved.
//

#import "Alert.h"

/****************************适配*********************************/
#define Width [UIScreen mainScreen].bounds.size.width
#define Height [UIScreen mainScreen].bounds.size.height
#define ratew (Width/375.0)
#define rateh (Height/667.0)
#define CGRect(x,y,width,heigth)  CGRectMake((x)*ratew, (y)*rateh, width*ratew, heigth*rateh)
/****************************适配*********************************/


@interface Alert ()

@end

@implementation Alert

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
/*
 提示框
 title  提示标题
 message    提示信息
 time   显示的时间长度
 */
+(void)alertTitle:(NSString *)title message:(NSString *)message timeInterval:(NSTimeInterval)time{
    id controller = [UIApplication  sharedApplication].delegate.window.rootViewController;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [controller presentViewController:alert animated:YES completion:nil];
    [NSTimer scheduledTimerWithTimeInterval:time target:self selector:@selector(dismissAlert) userInfo:nil repeats:NO];
}
/*
 提示框
 title  提示标题
 message    提示信息
 time   一定时间后返回数据
 */
+(void)alertTitle:(NSString *)title message:(NSString *)message withBlock:(resultBlock)block{
    id controller = [UIApplication  sharedApplication].delegate.window.rootViewController;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [controller presentViewController:alert animated:YES completion:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self dismissAlert];
        if (block!=nil) {
            block(@"ok");
        }
    });
}
/*
 提示框
 title  提示标题
 message    提示信息
 titleArr   显示的内容
 */
+(void)alertTitle:(NSString *)title message:(NSString *)message titleArr:(NSArray *)titleArr withBlock:(resultBlock)block{
    id controller = [UIApplication  sharedApplication].delegate.window.rootViewController;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    for (id obj in titleArr) {
        [alert addAction:[UIAlertAction actionWithTitle:obj style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (block) {
                block(obj);
            }
        }]];
    }
    [controller presentViewController:alert animated:YES completion:nil];
}
/*视图消失*/
+(void)dismissAlert{
    id controller = [UIApplication  sharedApplication].delegate.window.rootViewController;
    [controller dismissViewControllerAnimated:YES completion:nil];
}



/*sheet*/
+(void)actionSheet:(NSString *)title message:(NSString *)message titleArr:(NSArray *)titleArr withBlock:(resultBlock)block{
    id controller = [UIApplication  sharedApplication].delegate.window.rootViewController;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleActionSheet];
    for (id obj in titleArr) {
        if ([obj isEqualToString:@"取消"]||[obj isEqualToString:@"cancel"]) {
            [alert addAction:[UIAlertAction actionWithTitle:obj style:UIAlertActionStyleCancel handler:nil]];
        }else{
            [alert addAction:[UIAlertAction actionWithTitle:obj style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                block(obj);
            }]];
        }
    }
    [controller presentViewController:alert animated:YES completion:nil];
}
/*
 * 小弹框 1s后消失
 */
+(void)alertWithButtonTitle:(NSString*)title{
    id controller = [UIApplication  sharedApplication].delegate.window.rootViewController;
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [[controller view] addSubview:button];
    button.frame = CGRectMake((Width-100*rateh)/2, Height/2-50*rateh, 100*rateh, 50*rateh);
    button.layer.cornerRadius = 25*rateh;
    button.backgroundColor = [UIColor blackColor];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button sizeToFit];
    button.frame = CGRectMake((Width-button.frame.size.width-50*rateh)/2, Height/2-50*rateh, button.frame.size.width+50*rateh, 50*rateh);
    [UIView animateWithDuration:1 animations:^{
        button.alpha = 0;
    } completion:^(BOOL finished) {
        [button removeFromSuperview];
    }];
}




/*************非导航条下的视图弹框****************/
/*
 提示框
 title  提示标题
 message    提示信息
 controller 控制器
 time   一定时间后返回数据
 */
+(void)alertTitle:(NSString *)title message:(NSString *)message controller:(id)controller withBlock:(resultBlock)block{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [controller presentViewController:alert animated:YES completion:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
        if (block!=nil) {
            block(@"ok");
        }
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
