//
//  NSDate+Time.m
//  BSH
//
//  Created by 杨海波 on 2017/3/6.
//  Copyright © 2017年 hibo. All rights reserved.
//

#import "NSDate+Time.h"

@implementation NSDate (Time)


/*获取时间戳对应的 年、月、日、周的最小时间
 name:dateItemByString
 parameter：secs 时间戳
 parameter：type 时间转换方式
 return:    string
 将时间戳转换成对应的时间字符串类型
 */
+(NSTimeInterval)minTimeByTimeInterval:(NSTimeInterval)secs type:(DateType)type{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setTimeZone:nil];
    switch (type) {
        case DATE_TYPE_0:
            [formatter setDateFormat:@"yyyy"];
            break;
        case DATE_TYPE_1:
            [formatter setDateFormat:@"yyyy-MM"];
            break;
        case DATE_TYPE_2:
            [formatter setDateFormat:@"yyyy-MM-dd"];
            break;
        case DATE_TYPE_3:
            
            break;
        default:
            break;
    }
    if (type == DATE_TYPE_3) {
        NSTimeInterval week;
        week = ((int)secs/86400-3)%7;
        return week;
    }
    NSString *item = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:secs]];
    NSTimeInterval time = [[formatter dateFromString:item] timeIntervalSince1970];
    return time;
}


/*
 name:timeTurnString
 parameter：secs 时间戳
 parameter：type 时间转换方式
 return:    string
 将时间戳转换成对应的时间字符串类型
 */
+(NSString*)timeTurnString:(NSTimeInterval)secs{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setTimeZone:nil];
    NSTimeInterval minYear = [NSDate minTimeByTimeInterval:[[NSDate date] timeIntervalSince1970] type:DATE_TYPE_0];//今年的最小时间
    if (secs>minYear) {
        [formatter setDateFormat:@"MM-dd HH:mm"];
    }else{
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    }
    NSTimeInterval minDay = [NSDate minTimeByTimeInterval:[[NSDate date] timeIntervalSince1970] type:DATE_TYPE_2];//今天的最小时间
    NSString *timeStr = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:secs]];
    if (secs>minDay&&secs<minDay+86400) {
        [formatter setDateFormat:@"HH:mm"];
        timeStr = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:secs]];
        timeStr = [NSString stringWithFormat:@"今天 %@",timeStr];
    }else if (secs>minDay-86400&&secs<minDay) {
        [formatter setDateFormat:@"HH:mm"];
        timeStr = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:secs]];
        timeStr = [NSString stringWithFormat:@"昨天 %@",timeStr];
    }else if (secs>minDay-172800&&secs<minDay-86400) {
        [formatter setDateFormat:@"HH:mm"];
        timeStr = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:secs]];
        timeStr = [NSString stringWithFormat:@"前天 %@",timeStr];
    }
    return timeStr;
}
#pragma mark - 格式化时间
+(NSString*)dateFormat:(NSString*)format date:(NSTimeInterval)secs{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setTimeZone:nil];
    [formatter setDateFormat:format];
     NSString *timeStr = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:secs]];
    return timeStr;
}
#pragma mark - 判断这一天是周几
+(NSString*)getWeekDate:(NSTimeInterval)secs{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setTimeZone:nil];
    [formatter setDateFormat:@"EEEE"];
    NSString *timeStr = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:secs]];
    return timeStr;
}

#pragma mark - date
//根据日期求天
+ (NSInteger)day:(NSDate *)date{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date];
    return [components day];
}

//根据日期求月
+ (NSInteger)month:(NSDate *)date{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date];
    return [components month];
}

//根据日期求年
+ (NSInteger)year:(NSDate *)date{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date];
    return [components year];
}

//这个月的第一天是周几
+ (NSInteger)firstWeekdayInThisMonth:(NSDate *)date{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar setFirstWeekday:1];//1.Sun. 2.Mon. 3.Thes. 4.Wed. 5.Thur. 6.Fri. 7.Sat.
    NSDateComponents *comp = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date];
    [comp setDay:1];
    NSDate *firstDayOfMonthDate = [calendar dateFromComponents:comp];
    
    NSUInteger firstWeekday = [calendar ordinalityOfUnit:NSCalendarUnitWeekday inUnit:NSCalendarUnitWeekOfMonth forDate:firstDayOfMonthDate];
    return firstWeekday - 1;
}
//这个月一共有多少天
+ (NSInteger)totaldaysInThisMonth:(NSDate *)date{
    NSRange totaldaysInMonth = [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date];
    return totaldaysInMonth.length;
}

//上个月的这一天
+ (NSDate *)lastMonth:(NSDate *)date{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    dateComponents.month = -1;
    NSDate *newDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:date options:0];
    return newDate;
}
//下个月的这一天
+ (NSDate*)nextMonth:(NSDate *)date{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    dateComponents.month = +1;
    NSDate *newDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:date options:0];
    return newDate;
}

/*
 *  获取日周月的起始时间戳
 *  unit  NSCalendarUnitDay,NSCalendarUnitWeekOfMonth,NSCalendarUnitMonth
 */
+ (NSArray *)getBeginAndEndWith:(NSDate *)newDate rangeOfUnit:(NSUInteger)unit{
    if (newDate == nil) {
        newDate = [NSDate date];
    }
    double interval = 0;
    NSDate *beginDate = nil;
    NSDate *endDate = nil;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setFirstWeekday:2];//设定周一为周首日
    BOOL ok = [calendar rangeOfUnit:unit startDate:&beginDate interval:&interval forDate:newDate];
    //分别修改为 NSDayCalendarUnit NSWeekCalendarUnit NSYearCalendarUnit  上个星期NSCalendarUnitWeekOfMonth
    if (ok) {
        endDate = [beginDate dateByAddingTimeInterval:interval-1];
    }
    NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
    [myDateFormatter setDateFormat:@"yyyy.MM.dd"];
    NSString *mystartTime = [NSString stringWithFormat:@"%ld",(long)[beginDate timeIntervalSince1970]];
    NSString *myendTime   = [NSString stringWithFormat:@"%ld",(long)[endDate timeIntervalSince1970]];
    NSArray *array = @[mystartTime,myendTime];
    //NSLog(@"%ld,%ld",(long)[beginDate timeIntervalSince1970],(long)[endDate timeIntervalSince1970]);
    return array;
}
/****************************** 获取日周月的起始时间戳 ******************************/




@end
