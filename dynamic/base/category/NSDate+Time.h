//
//  NSDate+Time.h
//  BSH
//
//  Created by 杨海波 on 2017/3/6.
//  Copyright © 2017年 hibo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Time)

/*
 根据时间戳获取当前年、月、日
 */
//时间显示方式
typedef enum {
    DATE_TYPE_0 = 0,//年
    DATE_TYPE_1,    //月
    DATE_TYPE_2,    //日
    DATE_TYPE_3     //周
}DateType;
+(NSTimeInterval)minTimeByTimeInterval:(NSTimeInterval)secs type:(DateType)type;

/*
    根据时间戳获取对应的时间显示
 */
+(NSString*)timeTurnString:(NSTimeInterval)secs;
#pragma mark - 格式化时间
+(NSString*)dateFormat:(NSString*)format date:(NSTimeInterval)secs;
#pragma mark - 判断这一天是周几
+(NSString*)getWeekDate:(NSTimeInterval)secs;

/*
 * 获取日周月的起始时间戳
 * NSCalendarUnitMonth,NSCalendarUnitDay,NSCalendarUnitWeekOfMonth
 */
+ (NSArray *)getBeginAndEndWith:(NSDate *)newDate rangeOfUnit:(NSUInteger)unit;

/*
 * 根据日期求天
 */
+ (NSInteger)day:(NSDate *)date;

/*
 * 根据日期求月
 */
+ (NSInteger)month:(NSDate *)date;

/*
 * 根据日期求年
 */
+ (NSInteger)year:(NSDate *)date;

/*
 * 这个月的第一天是周几
 */
+ (NSInteger)firstWeekdayInThisMonth:(NSDate *)date;

/*
 * 求这个月的总天数
 */
+ (NSInteger)totaldaysInThisMonth:(NSDate *)date;


/*
 * 这天的最后一个月
 */
+ (NSDate *)lastMonth:(NSDate *)date;

/*
 * 下个月
 */
+ (NSDate*)nextMonth:(NSDate *)date;


@end
