//
//  OutputView.h
//  OutputView
//
//  Created by 杨海波 on 2017/3/15.
//  Copyright © 2017年 杨海波. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OutputDelegate <NSObject>

@required
- (void)didSelectedAtIndexPath:(NSIndexPath *)indexPath;//选择代理方法
@optional
- (void)didSelectedAtData:(id)result;
@end

//代码块
typedef void(^outputViewReturn)(id result);

typedef enum {
    //箭头方向 朝上
    OutputViewDirectionLeft = 0,//左
    OutputViewDirectionCenter,  //中
    OutputViewDirectionRight,    //右
    //箭头方向 朝下
    OutputViewDirectionDownLeft,    //左
    OutputViewDirectionDownCenter,  //中
    OutputViewDirectionDownRight,    //右
}Direction;

@interface OutputView : UIView<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,weak) id<OutputDelegate>delegate;
@property (nonatomic,strong) UITableView  *tableView;
@property (nonatomic,strong) NSArray *dataArray;//传入选择列表
@property (nonatomic,assign) NSTextAlignment textAlignment;//文本 居中，居右，居左选择
@property (nonatomic,strong) outputViewReturn resultBlock;//返回选择结果回调
@property (nonatomic,strong) outputViewReturn dismissBlock;//界面消失回调
@property(nonatomic,strong) UIColor *celltextColor;//cell颜色

//初始化
-(instancetype)initWithPoint:(CGPoint)origin
                   rowHeigth:(CGFloat)rowHeigth
                   direction:(Direction)direction
                   dataArray:(NSArray*)dataArray;
//设置弹框宽度
-(void)setRowWidth:(CGFloat)rowWidth;
//设置箭头位置左右偏移量
-(void)setArrow_x:(CGFloat)arrow_x;
//设置分割线颜色
-(void)setSeparatorColor:(UIColor*)separatorColor;
//弹出
-(void)pop;
//弹出两次动画
- (void)popForTwo;
//消失
-(void)dismiss;


@end
